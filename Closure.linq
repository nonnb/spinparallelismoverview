<Query Kind="Program" />

void Main()
{
	// someNumber is a local value type, so its on the stack, right?
	int someNumber = 10;
	Console.WriteLine(someNumber);
	
	// And now?
	Func<int> foo = () => someNumber;
}