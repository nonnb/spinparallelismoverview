﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace ClassLibrary1
{
    public class OldSchoolAsync
    {
        public void StartSomethingAsync(Action<string> callbackWhenDone)
        {
            var thread = new Thread(() =>
            {
                Thread.Sleep(1000);
                callbackWhenDone("Here's the data");
            });
            thread.Start();
        }
    }

    [TestFixture]
    public class AsyncDemo
    {
        [Test]
        public void InterfaceWithOldSchool()
        {
            var oldAsyncApi = new OldSchoolAsync();
            oldAsyncApi.StartSomethingAsync(data => Console.WriteLine(data));
            Console.WriteLine("Main thread done!");

            // Um, so what guarantees do we have
            // That the callback will ever happen? (e.g. Timeouts)
            // That the callback will happen exactly once, or more than once
            // What about exceptions ?
            // How can we pass additional context / data between "this" thread, and the callback, when it happens?

            // Hack, to stop R# test runner from tearing down the process.
            Thread.Sleep(2000);
        }

        [Test]
        public async Task BridgingOldSchoolUsingTaskCompletionSource()
        {
            // The Type of TCS is determined by the data returned in the callback
            var tcsAsyncDone = new TaskCompletionSource<string>();
            var oldAsyncApi = new OldSchoolAsync();
            oldAsyncApi.StartSomethingAsync(data =>
            {
                tcsAsyncDone.SetResult(data);
            });
            var result = await tcsAsyncDone.Task;
            Console.WriteLine(result);
        }

        // Who remembers jQuery function() call back lambdas ... ?
        [Test]
        public void ContinuationPassingStyle()
        {
            Console.WriteLine($"Starting on thread {Thread.CurrentThread.ManagedThreadId}");
            var tsk = Task.Delay(1000)
                .ContinueWith(t =>
                {
                    // TODO check t.IsFaulted
                    Console.WriteLine($"Continuing A on thread {Thread.CurrentThread.ManagedThreadId}");
                    Thread.Sleep(2000);
                    Task.Delay(1000)
                        .ContinueWith(t2 =>
                        {
                            // TODO check IsFaulted
                            Thread.Sleep(1000);
                            Console.WriteLine($"Continuing A.1 on thread {Thread.CurrentThread.ManagedThreadId}");
                        })
                    .Wait(); // Bad!
                })
                .ContinueWith(t =>
                {
                    Thread.Sleep(1000);
                    Console.WriteLine($"Continuing B on thread {Thread.CurrentThread.ManagedThreadId}");
                });
            // Don't!!!! In esp. UI, this can easily lead to deadlocks, if two or more tasks are contending for the ThreadSynchronizationContext
            tsk.Wait();
        }

        [Test]
        public async Task HoorayAsyncAwait()
        {
            try
            {
                await Task.Delay(1000);
                await Task.Delay(2000);
                // Avoid using .ConfigureAwait(false) - Standard / Core no longer carry thread synchronization context.
                // AsyncLocal<> seems to be the way forward if you DO need context
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Oops {ex}");
            }
        }

        [Test]
        public async Task AsyncParallelization()
        {
            var t1 = Task.Delay(1000);
            var t2 = Task.Delay(2000);

            // Task.WaitAll(t1, t2); .... What's the difference here?
            // Read : Stephen Cleary's : There IS no thread
            await Task.WhenAll(t1, t2);
        }

        // https://stackoverflow.com/questions/4238345/asynchronously-wait-for-taskt-to-complete-with-timeout
        [Test]
        public async Task AsyncTimeOutPattern()
        {
            var timeout = 1000; // ms
            var task = HoorayAsyncAwait();
            if (await Task.WhenAny(task, Task.Delay(timeout)) == task)
            {
                // task completed within timeout
            }
            else
            {
                // timeout logic
            }
        }

        // If our method is asynchronous, but isn't doing any value add AFTER a task which returns the data we need, then just return the Task!
        [Test]
        public Task ReturnATaskIfNoAdditionalWork()
        {
            var t1 = Task.Delay(1000);
            var t2 = Task.Delay(2000);

            // i.e. no need for the Compiler synctactic sugar statemachine re-write
            return Task.WhenAll(t1, t2);
        }

        [Test]
        public Task<string> ButNotIfTheresDisposablesInvolved()
        {
            using (var fooFile = File.OpenText(@"c:\temp\foos.txt"))
            {
                return fooFile.ReadToEndAsync();
            }
            // So, what's the problem ... ?
        }

        [Test]
        public void FireAndForget()
        {
#pragma warning disable 4014 // Please tell the next guy all about why you have chosen this approach
            Task.Delay(1000)
                .ContinueWith(x => Task.Delay(1000));
#pragma warning restore 4014
            // Stephen Cleary doesn't like this much .. especially in ASP.Net ... the continuation needs to get a Thread from the Threadpool - interferes
            // Largely seen as an anti pattern
        }

        public async Task BothCpuBoundAndIoBoundMethod()
        {
            Thread.Sleep(1000);
            Console.WriteLine($"Finished Sleeping on {Thread.CurrentThread.ManagedThreadId}");
            await Task.Delay(1000);
            Console.WriteLine($"Finished Delaying on {Thread.CurrentThread.ManagedThreadId}");
        }

        [Test]
        public async Task SoExactlyWhatIsAsync1()
        {
            Console.WriteLine($"Started on {Thread.CurrentThread.ManagedThreadId}");
            await BothCpuBoundAndIoBoundMethod();
            Console.WriteLine($"Main thread completed on {Thread.CurrentThread.ManagedThreadId}");
        }

        [Test]
        public void SoExactlyWhatIsAsync2()
        {
            Console.WriteLine($"Started on {Thread.CurrentThread.ManagedThreadId}");
#pragma warning disable 4014 // Yup, fire and forget. But does this come back immediately?
            var t = BothCpuBoundAndIoBoundMethod();
#pragma warning restore 4014
            Console.WriteLine($"Main thread completed on {Thread.CurrentThread.ManagedThreadId}");

            // This is just for R# to keep the process alive. Ignore
            t.Wait();
        }

        [Test]
        public void SoExactlyWhatIsAsync3()
        {
            Console.WriteLine($"Started on {Thread.CurrentThread.ManagedThreadId}");
#pragma warning disable 4014 // And now?
            var t = Task.Run(() => BothCpuBoundAndIoBoundMethod());  // Janus favourite hammer :)
#pragma warning restore 4014
            Console.WriteLine($"Main thread completed on {Thread.CurrentThread.ManagedThreadId}");

            // This is just for R# to keep the process alive. Ignore
            t.Wait();
        }
    }
}
