using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace ClassLibrary1
{
    public static class Helpers
    {
        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> items)
        {
            return new HashSet<T>(items);
        }
    }


    [TestFixture]
    public class ThreadSafetyDemo
    {
        private const int NumCoresThisPc = 4;
        private const int NumThreads = NumCoresThisPc * 2;

        private static void AssignThreadToProcessor(int affinityMask)
        {
            Thread.BeginThreadAffinity();
#pragma warning disable 618 // Yes, I know what I'm doing ...
            var osThreadId = AppDomain.GetCurrentThreadId();
#pragma warning restore 618
            var thisProcessThread = Process.GetCurrentProcess()
                .Threads
                .Cast<ProcessThread>()
                .Single(t => t.Id == osThreadId);
            thisProcessThread.IdealProcessor = 0;
            thisProcessThread.ProcessorAffinity = (IntPtr)affinityMask;
        }

        [Test]
        public void _00ACpuBoundWork()
        {
            // The affinity mask is a flags enum (1 = CPU0, 2 = CPU1, 4 = CPU2 ...)
            AssignThreadToProcessor(0x0004);
            for (var denom = 1; denom < int.MaxValue; denom++)
            {
                var total = 0d;
                for (var num = 0; num < denom; num++)
                {
                    total += (double)num / denom;
                }
                var average = total / denom;
            }
        }

        [Test]
        public void _00BalancedWork()
        {
            Parallel.For(1, int.MaxValue, denom =>
            {
                var total = 0d;
                for (var num = 0; num < denom; num++)
                {
                    total += (double)num / denom;
                }
                var average = total / denom;
            });
        }


        // No contention for any resource, no mutation
        static void DoUncontendedCpuBoundWork(int i)
        {
            Console.WriteLine("Work Item {0} started on Thread {1}", i, Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(5000);
            Console.WriteLine("Work Item {0} finished on Thread {1}", i, Thread.CurrentThread.ManagedThreadId);
        }

        [Test]
        public void _01UncontendedParallelism()
        {
            var myThreads = new List<Thread>();
            foreach (var i in Enumerable.Range(0, NumThreads))
            {
                var thread = new Thread(() => DoUncontendedCpuBoundWork(i));
                myThreads.Add(thread);
            }
            foreach (var thread in myThreads)
            {
                thread.Start();
            }
            // How many threads do I have right now?
            foreach (var thread in myThreads)
            {
                thread.Join();
            }
            // All work starts ~simultaneously, and each finishes on the same thread it started on

            //            Enumerable.Range(0, NumThreads)
            //                .ToList()
            //                .ForEach(i =>

        }

        private readonly object _myLock = new object();
        // private int _myLock = 0;
        // private const int _myLock = 0;
        void DoContendedWork(int i)
        {
            Console.WriteLine("Work Item {0} on Thread {1} waiting for lock",
                i, Thread.CurrentThread.ManagedThreadId);
            lock (_myLock)
            {
                Console.WriteLine("Work Item {0} got the lock!", i);
                Thread.Sleep(2000);
            }
            Console.WriteLine("Work Item {0} finished on Thread {1}",
                i, Thread.CurrentThread.ManagedThreadId);

        }

        [Test]
        public void _02ShowBlockingForLock()
        {
            var myThreads = new List<Thread>();
            Enumerable.Range(0, NumThreads)
                .ToList()
                .ForEach(i =>
                {
                    var x = i;
                    var thread = new Thread(() => DoContendedWork(x));
                    thread.Start();
                    myThreads.Add(thread);
                });
            foreach (var thread in myThreads)
            {
                thread.Join();
            }
            // Benefits over single threaded DoContendedWork?
            // lock syntactic sugar is safer than Monitor.Enter (implicit try finally, check for reference type)
            // Why should lock variables be private, immutable reference types?
        }

        private readonly object _lock1 = new object();
        private readonly object _lock2 = new object();

        void DoContendedWorkL1L2(int i)
        {
            Console.WriteLine("Work Item {0} on Thread {1} trying to acquire lock1",
                i, Thread.CurrentThread.ManagedThreadId);
            Monitor.Enter(_lock1);

            Console.WriteLine("Work Item {0} got lock1!", i);
            Thread.Sleep(1000);

            Console.WriteLine("Work Item {0} on Thread {1} trying to acquire lock2",
                i, Thread.CurrentThread.ManagedThreadId);
            Monitor.Enter(_lock2);
            Console.WriteLine("Work Item {0} got lock2!", i);
            Thread.Sleep(1000);

            Monitor.Exit(_lock2);
            Monitor.Exit(_lock1);
            Console.WriteLine("Work Item {0} finished on Thread {1}",
                i, Thread.CurrentThread.ManagedThreadId);
        }

        void DoContendedWorkL2L1(int i)
        {
            Console.WriteLine("Work Item {0} on Thread {1} trying to acquire lock2",
                i, Thread.CurrentThread.ManagedThreadId);
            Monitor.Enter(_lock2);

            Console.WriteLine("Work Item {0} got lock2!", i);
            Thread.Sleep(1000);

            Console.WriteLine("Work Item {0} on Thread {1} trying to acquire lock1",
                i, Thread.CurrentThread.ManagedThreadId);
            Monitor.Enter(_lock1);
            Console.WriteLine("Work Item {0} got lock1!", i);
            Thread.Sleep(1000);

            Monitor.Exit(_lock1);
            Monitor.Exit(_lock2);
            Console.WriteLine("Work Item {0} finished on Thread {1}",
                i, Thread.CurrentThread.ManagedThreadId);
        }

        [Test]
        public void _02ADeadlock()
        {
            var thread1 = new Thread(() => DoContendedWorkL1L2(1));
            var thread2 = new Thread(() => DoContendedWorkL2L1(2));
            thread1.Start();
            thread2.Start();
            thread1.Join();
            thread2.Join();
            // Sharing locks is a bad idea (never public, never this)
            // Timeouts?
            // Locks are a smell ...
        }

        private int _sharedValue = 0; // Variable shared between all threads
        private const int NumLoops = 100000;
        public void LockFreeMutation()
        {
            for (var loop = 0; loop < NumLoops; loop++)
            {
                _sharedValue = _sharedValue + 1;
                // _sharedValue++;
                // Thread.MemoryBarrier();
                // _sharedValue++;
                // Interlocked.Increment(ref _sharedValue);
            }
        }

        [Test]
        public void _03DataRaceConditionIssue()
        {
            var myThreads = new List<Thread>();
            for (var i = 0; i < NumThreads; i++)
            {
                var thread = new Thread(LockFreeMutation);
                thread.Start();
                myThreads.Add(thread);
            }
            foreach (var thread in myThreads)
            {
                thread.Join();
            }
            Assert.AreEqual(NumThreads * NumLoops, _sharedValue);
            // Our mindset is still 'wrong' here - N threads concurrently trying to mutate the same thing is the smell.
            // Immutability is usually the cure
        }

        // Is this thread safe?
        public delegate void MyEvent();
        public MyEvent TheEvent;
        public void RaiseEventIfWeHaveSubscriber()
        {
            if (TheEvent != null)
            {
                TheEvent();
            }
            // TheEvent?.Invoke();
            // The (anti) pattern here is reasoning over a variable and then assuming that it is immutable
            // afterwards
            // R# recommends Elvis for good reason. IMO Elvis and side effecting Actions are a good thing
        }

        private static void DoLoopedAction(long numIterations, Action someMutation)
        {
            for (var loop = 0L; loop < numIterations; loop++)
            {
                someMutation();
            }
        }

        private static void NonAtomicReaderAsserter(Action someAssertion)
        {
            try
            {
                while (true)
                {
                    someAssertion();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Oops - NonAtomic - {ex.Message}");
                // Who catches?
                throw;
            }
        }

        public class MyClass
        {
            public int SomeInt { get; set; }
            public bool SomeBool { get; set; }
        }

        // Two separate variables, mutated inpdependently without a lock
        [Test]
        public void _04ShowUnsafeNonAtomicOperation()
        {
            const long numIterations = 10000000L;
            var theObj = new MyClass();
            var writerThread1 = new Thread(() => DoLoopedAction(numIterations, () =>
            {
                theObj.SomeInt = -1;
                theObj.SomeBool = true;
            }));
            var writerThread2 = new Thread(() => DoLoopedAction(numIterations, () =>
            {
                theObj.SomeInt = 0;
                theObj.SomeBool = false;
            }));
            bool atomic = true;
            var readerThread = new Thread(
                () => NonAtomicReaderAsserter(() =>
                {
                    var theBool = theObj.SomeBool;
                    var theInt = theObj.SomeInt;
                    atomic = (theBool == false && theInt == 0
                                     || theBool == true && theInt == -1);
                    Assert.True(atomic, "Oops - was expecting either -1 and true or 0 and false");
                }))
            {
                IsBackground = true
            };
            writerThread1.Start();
            writerThread2.Start();
            readerThread.Start();

            writerThread1.Join();
            writerThread2.Join();
            Assert.True(atomic);
            // OK, that's pretty obvious - even on a single core, context switching at the wrong time would
            // break the atomicity
        }

        [Test]
        public void _05ShowUnsafeNonAtomicOperation()
        {
            const long numIterations = 100000000L;
            var guid1 = new Guid("ffffffff-ffff-ffff-ffff-ffffffffffff");
            var guid2 = new Guid("00000000-0000-0000-0000-000000000000");
            var theGuid = guid1;
            var writerThread1 = new Thread(() => DoLoopedAction(numIterations, () =>
            {
                theGuid = guid1;
            }));
            var writerThread2 = new Thread(() => DoLoopedAction(numIterations, () =>
            {
                theGuid = guid2;
            }));
            bool atomic = true;
            var readerThread = new Thread(
                () => NonAtomicReaderAsserter(() =>
                {
                    var readValue = theGuid;
                    atomic = readValue == guid1 || readValue == guid2;
                    Assert.True(atomic,
                        $"I read {readValue}");
                }))
            {
                IsBackground = true
            };
            writerThread1.Start();
            writerThread2.Start();
            readerThread.Start();

            writerThread1.Join();
            writerThread2.Join();
            Assert.True(atomic);

            // So what exactly does a 64 bit processor mean?
            // Which primitives and structs are atomic ?
            // (decimal, DateTime, ...)
            // Reference types are however atomic (64 bit `pointers` / selectors)
            // We're still on the wrong track -> Mutability is the enemy
        }

        // Right. So reference types are atomic. But are they thread safe?
        // Pretend that this class is really expensive to create. We don't want to create it unnecessarily
        private MyClass _myClass = null;
        private MyClass CachedPropGetterMk1
        {
            get
            {
                // Recognize the anti pattern?
                if (_myClass == null)
                {
                    // Oops - what if two threads wind up here simultaneously?
                    _myClass = new MyClass();
                }
                return _myClass;
                // One or more of these "MyClass" instances will be orphaned from the owner class
                // Guaranteed to provide hours of debugging fun.

                // Problem - Although we only want _myClass to be initialized at most once, it can't be 
                // readonly since it has two values - null, and instance
            }
        }

        // Lets use our lock `Mjolnir`
        private readonly object _myPropLocker = new object();
        private MyClass CachedPropGetterMk2Safer
        {
            get
            {
                // But, if the getter is in a tight loop, this will be highly contended / wasteful?
                // lock is only needed on the first (possibly contended) call
                lock (_myPropLocker)
                {
                    if (_myClass == null)
                    {
                        _myClass = new MyClass();
                    }
                }
                // Most of the time, the getter is perfectly thread safe and doesn't need the lock ...
                return _myClass;
            }
            // Is the smell again doing mutation inside a `getter` which is usually readonly ...
        }

        private MyClass CachedPropGetterMk3DoubleChecked
        {
            get
            {
                // With the proviso that we will never want to set this back to NULL
                if (_myClass == null)
                {
                    lock (_myPropLocker)
                    {
                        if (_myClass == null)
                        {
                            _myClass = new MyClass();
                        }
                    }
                }
                return _myClass;
            }
            // double checked locking is becoming regarded as an antipattern, due to various compiler and processor optimisations
        }

        // Rule : Don't do any of the above. Use the framework.
        // Lazy<T> offers overloads for factory method, and ThreadSafety options
        private readonly Lazy<MyClass> _myLazyClassInstance = new Lazy<MyClass>();

        [Test]
        public void _06DoubleCheckLockLazyInit()
        {
            // There is a small price to pay ... accesses now require .Value
            var value = _myLazyClassInstance.Value;
        }

        // A task is not a thread. A task needs to be scheduled to a thread
        // Parallel.For / ForEach / AsParallel() doesn't spawn a new task per item - it partitions items them into tasks each with processing a subset of items
        // Also, Tasks can be serialized on a single thread - a task will only be scheduled once a thread is free
        [Test]
        public void _10TaskThreadsAndPartitioning()
        {
            Parallel.ForEach(Enumerable.Range(0, 500),
                new ParallelOptions
                {
                    MaxDegreeOfParallelism = 8
                },
                i =>
                {
                    Console.WriteLine("Starting work item {0} on Task {1} on Thread {2}", i, Task.CurrentId,
                        Thread.CurrentThread.ManagedThreadId);
                    Thread.Sleep(10);
                    Console.WriteLine("Completed work item {0} on Task {1} on Thread {2}", i, Task.CurrentId,
                        Thread.CurrentThread.ManagedThreadId);
                });
            // Pedantic, but would suggest Parallel.ForEach for side effecting code and
            // .AsParallel() for (immutable) projections
        }

        public void SleepOneSecondSync()
        {
            Debug.WriteLine("Start Thread {0} Task {1}", Thread.CurrentThread.ManagedThreadId, Task.CurrentId);
            Thread.Sleep(1000);
            Debug.WriteLine("End Thread {0} Task {1}", Thread.CurrentThread.ManagedThreadId, Task.CurrentId);
        }

        [TestCase(1, 100)]// 1 is ignored ... Can't Max threads < cores. i4790 is a quad core x 2 HT
        [TestCase(8, 100)] // Oo - why only 7 Threads created?
        [TestCase(100, 100)]
        // Toggle release mode
        [TestCase(1000, 1000)]
        [TestCase(10000, 10000)] // Ha! Who needs async
        [TestCase(100000, 100000)] // Watch the actual number of threads - value is ignored
        public void _11TaskSchedulerThreadPoolAndStarvation(int numThreads, int numTasks)
        {
            ThreadPool.SetMaxThreads(numThreads, numThreads);
            ThreadPool.SetMinThreads(numThreads, numThreads);

            var tasks = new List<Task>();
            for (var loop = 0; loop < numTasks; loop++)
            {
                // Some I/O bound operation which takes 1 second, e.g. a Web Service call or a DB query
                tasks.Add(Task.Run(() => SleepOneSecondSync()));
            }
            Task.WaitAll(tasks.ToArray());
        }

        public async Task SleepOneSecondASync(int taskId)
        {
            Debug.WriteLine("Start Thread {0} Task {1}", Thread.CurrentThread.ManagedThreadId, taskId);
            await Task.Delay(1000).ConfigureAwait(continueOnCapturedContext: false);
            Debug.WriteLine("End Thread {0} Task {1}", Thread.CurrentThread.ManagedThreadId, taskId);
        }

        [TestCase(100)]
        [TestCase(1000)]
        [TestCase(10000)]
        [TestCase(100000)]
        [TestCase(1000000)]
        public async Task _12AsyncAwait(int numTasks)
        {
            // Does the number of IO Completion Port threads actually limit the number of concurrent async operations?
            ThreadPool.SetMaxThreads(4, 4);
            ThreadPool.SetMinThreads(4, 4);

            var tasks = new List<Task>();
            for (var loop = 0; loop < numTasks; loop++)
            {
                tasks.Add(SleepOneSecondASync(loop));
            }
            await Task.WhenAll(tasks);
        }

        [TestCase(1, 100)]
        [TestCase(4, 100)]
        [TestCase(8, 100)]
        [TestCase(100, 100)]
        [TestCase(1000, 1000)]
        [TestCase(1000000, 1000000)]
        public async Task _13AsyncAwaitLinquified(int numThreads, int numTasks)
        {
            // Leave threadpool management to the FW
            var tasks = Enumerable.Range(0, numTasks)
                // .AsParallel() // hehe
                .Select(SleepOneSecondASync);

            await Task.WhenAll(tasks)
                .ConfigureAwait(false);
        }
    }
}